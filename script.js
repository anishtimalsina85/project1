<div class="news-slider">
    <button class="slider-btn prev-btn">.</button>
    <div class="news-cards-container">
        <div class="news-card" style="display: block;">...</div>
        <div class="news-card" style="display: none;">...</div>
        <div class="news-card" style="display: none;">...</div>
        <!-- Add as many news cards as you have -->
    </div>
    <button class="slider-btn next-btn">.</button>
</div>

document.addEventListener('DOMContentLoaded', () => {
  const prevButton = document.querySelector('.prev-btn');
  const nextButton = document.querySelector('.next-btn');
  const newsCards = document.querySelectorAll('.news-card');

  let currentIndex = 0; // Track the current index

  const showSlide = (index) => {
      // Hide all slides
      newsCards.forEach(card => card.style.display = 'none');
      // Show the targeted slide
      newsCards[index].style.display = 'block';
  };

  // Show initial slide
  showSlide(currentIndex);

  // Show the next slide
  const showNextSlide = () => {
      currentIndex = (currentIndex + 1) % newsCards.length; // Go to next slide or wrap around
      showSlide(currentIndex);
  };

  // Show the previous slide
  const showPrevSlide = () => {
      currentIndex = (currentIndex - 1 + newsCards.length) % newsCards.length; // Go to previous slide or wrap around
      showSlide(currentIndex);
  };

  // Attach event listeners
  prevButton.addEventListener('click', showPrevSlide);
  nextButton.addEventListener('click', showNextSlide);
});